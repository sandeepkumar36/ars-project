<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html ng-app="myApp">
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css">
        

<style type="text/css">

/*  */
.form-bg {
	background-size: cover;
	position: relative;
}

.form-bg:before {
	content: "";
	width: 100%;
	height: 50%;
	background: rgba(73, 116, 130, 0.9);
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0.8;
}

.form-horizontal {
	padding: 60px 40px 55px 40px;
	background: #a0c5c508;
	border-radius: 10px;
}

.form-horizontal:before {
	/* content: "\f007"; */
	font-family: "Font Awesome 5 Free";
	font-weight: 900;
	width: 100px;
	height: 100px;
	line-height: 96px;
	border-radius: 50%;
	border: 4px solid #fff;
	background: #14a3aa;
	font-size: 40px;
	color: #fff;
	/* text-align: center; */
	margin: 0 auto;
	position: absolute;
	top: -10px;
	left: 0;
	right: 0;
}

.form-horizontal .heading {
	display: block;
	font-size: 28px;
	color: #597886;
	text-transform: capitalize;
	text-align: center;
	margin-bottom: 20px;
}

.form-horizontal .form-group {
	margin: 0 0 30px 0;
	position: relative;
}

.form-horizontal .form-group:last-child {
	margin: 0;
}

.form-horizontal .form-control {
	height: 50px;
	border: 2px solid #cbe9ea;
	border-radius: 5px;
	box-shadow: none;
	padding: 0 20px 0 26%;
	font-size: 16px;
	font-weight: bold;
	color: #94abb6;
	position: relative;
	transition: all 0.3s ease 0s;
}

.form-horizontal .form-control:focus {
	box-shadow: none;
	outline: 0 none;
}

.form-horizontal .control-label {
	width: 25%;
	height: 46px;
	line-height: 46px;
	background: #f5ffff;
	padding: 0;
	font-size: 16px;
	font-weight: bold;
	color: #94abb6;
	text-transform: capitalize;
	text-align: center;
	border-right: 2px solid #cbe9ea;
	position: absolute;
	top: 2px;
	left: 2px;
	z-index: 1;
}

.form-horizontal .btn, .form-horizontal .btn:focus {
	width: 100%;
	height: 50px;
	line-height: 50px;
	padding: 0 30px;
	background: #f5ffff;
	border: none;
	border-radius: 6px;
	font-size: 18px;
	font-weight: bold;
	color: #94abb6;
	text-transform: uppercase;
	position: relative;
}

.form-horizontal .btn:hover {
	background: #14a3aa;
}

.form-horizontal .btn:before {
	font-family: "Font Awesome 5 Free";
	font-weight: 900;
	position: absolute;
	top: 0;
	left: 30px;
	font-size: 24px;
	color: #fff;
}

.form-horizontal .signup, .form-horizontal .forgot-pass {
	display: inline-block;
	font-size: 17px;
	font-weight: bold;
	color: #14a3aa;
}

.form-horizontal .forgot-pass {
	float: right;
	color: #58aebc;
}

@media only screen and (max-width: 990px) {
	.form-horizontal:before {
		top: -50px;
	}
}

@media only screen and (max-width: 480px) {
	.form-horizontal {
		padding: 60px 20px 40px 20px;
	}
	.form-horizontal .control-label {
		font-size: 12px;
	}
}
.centered {
  position: absolute;
  left: 50%;
transform: translate( -50%);
}

.open>.dropdown-menu {
  height: 400% !important;
  
  width : 100%;
}
</style>

<title>Welcome</title>
</head>
<body ng-controller="ApprovalController">
<!-- 
 <div class="form-bg"> -->
		 <div class="container " style="margin-top: 10px;" >
			<div class="card centered" style="border: 1px solid;border-radius:30px;width: 60rem;height:72rem;">
				<!-- <div class="col-md-10">  -->
					<form class="form-horizontal" style="margin-top:-40px;" novalidate name="templateForm">
						<span class="heading">Template Config</span>

						<div class="form-group">
						  <label class="control-label" for="exampleInputName2">Process Code</label> 
						  <input type="text" class="form-control" id="inputpc"
								name="processCode" ng-model="approvalTemplateConfig.processCode"
								required>
						</div>

						<div class="form-group">
							<label for="duration" class="control-label">Duration</label> <input
								type="number" class="form-control" id="duration" name="duration"
								ng-model="approvalTemplateConfig.duration" required>
						</div>

						<div class="form-group">
							<label for="filename" class="control-label">File Name</label> 
							<input type="text" class="form-control" id="filename" name="filename"
								ng-model="approvalTemplateConfig.fileName" required>
						</div>

						<div class="form-group">
							<label for="inputSubject" class="control-label">Mail Subject</label> 
							<input type="text" class="form-control" id="inputSubject" name="subject"
							ng-model="approvalTemplateConfig.subject" required>
						</div>
						
						<div class="form-group">
							<label for="email" class="control-label">Email</label>
							 <input type="text" class="form-control" id="email" ng-model="approvalTemplateConfig.recievers" required>
						</div>
						<div class="form-group">
						  <div style="border: 2px #cbe9ea solid;width:38%;" ng-dropdown-multiselect="" options="timerArray" selected-model="selectedTime"
	                      extra-settings="selected_baseline_settings" translation-texts="selected_baselines_customTexts"></div>
						</div>
							<div class="form-group">
						
							<label for="status" style="border: 2px #cbe9ea solid;" class="control-label">Status</label>
							 <input type="checkbox" id="status" style="margin-left: 33%;width: 20px;" class="form-control" checked name="status" ng-model="approvalTemplateConfig.status"> 
						</div>
						<button ng-click="templateForm.$valid && submitConfig()" style="background-color:#5bc0de;color:#fff;" class="btn">Submit</button>
					</form>
				</div>
			</div>
  
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-dropdown-multiselect/2.0.0-beta.10/src/angularjs-dropdown-multiselect.js"></script>
    
	<script type="text/javascript">
		var app = angular.module('myApp', ['angularjs-dropdown-multiselect']);
		
		app.controller('ApprovalController', [
				'$scope','$filter',
				function(scope,$filter) {
					scope.timerArray = [];
					var j=1;
					for(var i=1;i<=24;i++){
						scope.timerArray.push({time:(i == 12 || i == 24 ? 12: i%12) +':00' +(i<13 ? ' am' : ' pm') ,id:j,serverTime:i+':00'});
						scope.timerArray.push({time:(i == 12 || i == 24 ? 12 : i%12)+':30' +(i<13 ? ' am' : ' pm') ,id:++j,serverTime:i+':30'});
						++j;
					}
					scope.arr = [];
					scope.approvalTemplateConfig = {};
					scope.timeString = undefined;
					init();
					function init() {
						scope.selectedTime = [];
						scope.selected_baseline_settings = {
							      	template: '<b>{{option.time}}</b>',
							      	selectionLimit: 4
							       
							      /*   selectedToTop: true, // Doesn't work
							        searchField: 'time',
							        enableSearch: true,
							        selectionLimit: 4 */
							      };
						scope.selected_baselines_customTexts = {buttonDefaultText: 'Select Time'};
					}
					
					scope.saveTime = function(){
						scope.timeString = "";
						scope.arr.push({timeForEmail : $filter('date')(scope.mailTime,'HH:mm') });
						for(var i=0;i<scope.arr.length;i++){
							scope.timeString +=	scope.arr[i].timeForEmail + ','
						}
						
						scope.timeString.substring(0,scope.timeString.length-2);
						scope.mailTime = undefined;	
					}

					scope.submitConfig = function() {
						scope.timeString = "";
						scope.time = [];
						for(var i=0;i<scope.selectedTime.length;i++){
							scope.time.push({timeForEmail : scope.selectedTime[i].serverTime});
						}
					scope.approvalTemplateConfig.mailTimes = scope.time;
					processRequest("/automatereportsystem/appConfig",scope.approvalTemplateConfig, function(data) {
							scope.approvalTemplateConfig = {};
							scope.arr = [];
							location.reload(true);
					 });
					}

					function processRequest(url, data, callback) {
						$.ajax({
							url : url,
							headers : {
								'Content-Type' : 'application/json'
							},
							method : 'POST',
							dataType : 'json',
							data : JSON.stringify(data),
							success : function(result) {
								callback(JSON.parse(result));
							}
						});
					}
				} ]);
	</script>
</body>
</html>
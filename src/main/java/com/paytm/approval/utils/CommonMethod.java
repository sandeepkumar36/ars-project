package com.paytm.approval.utils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class CommonMethod {

	static Logger logger = LoggerFactory.getLogger(CommonMethod.class);

	@SuppressWarnings("unchecked")
	public static Map<String, Object> hitThirdPartyUrl(String url) {

		OkHttpClient client = new OkHttpClient();
		Map<String, Object> urlResponse = new LinkedHashMap<String, Object>();
		Request request = new Request.Builder().url(url).get().addHeader("Content-Type", "application/json").build();

		try {
			Response response = client.newCall(request).execute();

			String result = response.body().string();
			logger.info("REQUEST: " + url + " ##RESPONSE: " + result);
			Gson gson = new Gson();
			urlResponse = gson.fromJson(result, urlResponse.getClass());

		} catch (IOException e) {
			
			e.printStackTrace();
		}

		logger.info("FINAL_RESPONSE_RETURN:: " + urlResponse);
		return urlResponse;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> hitPostThirdPartyUrl(String url, String requestBody) {

		OkHttpClient client = new OkHttpClient();
		Map<String, Object> urlResponse = new LinkedHashMap<String, Object>();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, requestBody);
		Request request = new Request.Builder().url(url).post(body).addHeader("Content-Type", "application/json")
				.build();

		try {
			Response response = client.newCall(request).execute();

			String result = response.body().string();
			logger.info("REQUEST: " + url + " ##RequestBody:" + requestBody + " ##RESPONSE: " + result);
			Gson gson = new Gson();
			urlResponse = gson.fromJson(result, urlResponse.getClass());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return urlResponse;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> uploadFile(String url, File file) {
		OkHttpClient client = new OkHttpClient();
		Map<String, Object> urlResponse = new LinkedHashMap<String, Object>();
		RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
				.addFormDataPart("media", file.getName(), RequestBody.create(MediaType.parse("file"), file))
				.addFormDataPart("some-field", "some-value").build();

		Request request = new Request.Builder().url(url).post(requestBody).build();

		try {
			Response response = client.newCall(request).execute();

			String result = response.body().string();
			logger.info("REQUEST: " + url + " ##RequestBody:" + requestBody + " ##RESPONSE: " + result);
			Gson gson = new Gson();
			urlResponse = gson.fromJson(result, urlResponse.getClass());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return urlResponse;
	}
}

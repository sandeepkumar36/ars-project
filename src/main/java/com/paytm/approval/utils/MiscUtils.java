package com.paytm.approval.utils;

import java.util.HashMap;
import java.util.Map;

public class MiscUtils {
	
	public final static Map<String, Object> getSuccessResponse(String message, Object data)
	{
	 	Map<String, Object> map=new HashMap<String,Object>();
	 	map.put("code",200);
	 	map.put("message",message);
	 	map.put("status", "success");
	 	map.put("data", data);
	 	return map;
	}

	public final static Map<String,Object>  getErrorResponse(String message)
	{
		Map<String,Object> map= new HashMap<String, Object>();
		map.put("errCode", 400);
		map.put("status", "error");
		map.put("message", message);
		return map;
	}

}

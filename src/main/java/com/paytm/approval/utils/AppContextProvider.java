package com.paytm.approval.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

public class AppContextProvider /*implements ApplicationContextAware*/{
	
	private static ApplicationContext appContext;

	//@Override
	public static void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		AppContextProvider.appContext=applicationContext;
		
	}
	
	public static ApplicationContext getAppContext() {
		return appContext;
	}

}

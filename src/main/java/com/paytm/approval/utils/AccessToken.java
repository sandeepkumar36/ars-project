package com.paytm.approval.utils;

import java.util.Map;

import com.paytm.approval.service.AccessTokenService;

public class AccessToken {

	private static AccessToken accessToken;
	private String token;
	private long expiryTime;

	private AccessToken() {}

	public static AccessToken getInstance() {
		if (accessToken == null || System.currentTimeMillis() >= accessToken.expiryTime ) {
			accessToken = new AccessToken();
			setTokenDetail(accessToken);
		} 
		return accessToken;
	}

	private static void setTokenDetail(AccessToken accessToken) {
		Map<String, Object> map = AppContextProvider.getAppContext().getBean(AccessTokenService.class).getAccessToken();
		accessToken.token = (String) map.get("access_token");
		accessToken.expiryTime = System.currentTimeMillis() + 7200 * 1000;
	}

	public String getToken() {
		return token;
	}
}

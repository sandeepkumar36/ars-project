package com.paytm.approval.constant;

public interface MessageConstant {
	
	int SUCCESS_CODE=200;
	String SUCCESS_MESSAGE="success";
	
	int ERROR_CODE=400;
	String ERROR_MESSAGE="ERROR";
	
	String DEPARTMENT_LIST_FETCHED="Department List Fetched Successfully.";

}

package com.paytm.approval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.paytm.approval.utils.AppContextProvider;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan(basePackages = "com.paytm.approval")
@EnableScheduling
public class AutomatereportsystemApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(AutomatereportsystemApplication.class, args);
		AppContextProvider.setApplicationContext(context);
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AutomatereportsystemApplication.class);

	}
}

package com.paytm.approval.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APPROVAL_TEMPLATE_CONFIG")
public class ApprovalTemplateConfig {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "APPROVAL_TEMPLATE_CONFIG_ID")
	private long approvalTemplateConfigId;

	@Column(name = "PROCESS_CODE", length = 500, nullable = false)
	private String processCode;

	@Column(name = "DEPARTMENT_ID")
	private long departmentId;

	@Column(name = "STATUS")
	private boolean status;

	@Column(name = "DURATION")
	private int duration;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "SUBJECT")
	private String subject;

	@Column(name = "recievers", length = 200)
	private String recievers;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "APPROVAL_TEMPLATE_CONFIG_ID")
	List<ApprovalEmailTime> mailTimes = new ArrayList<>();

	public long getApprovalTemplateConfigId() {
		return approvalTemplateConfigId;
	}

	public void setApprovalTemplateConfigId(long approvalTemplateConfigId) {
		this.approvalTemplateConfigId = approvalTemplateConfigId;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getRecievers() {
		return recievers;
	}

	public void setRecievers(String recievers) {
		this.recievers = recievers;
	}

	public List<ApprovalEmailTime> getMailTimes() {
		return mailTimes;
	}

	public void setMailTimes(List<ApprovalEmailTime> mailTimes) {
		this.mailTimes = mailTimes;
	}
}


package com.paytm.approval.pojo;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/*
 * pojo class for storing approval data for each approval instance
 * 
 * @author sandeep created time will be IST based
 */
@Entity
@Table(name = "APPROVAL_TEMPLATE_DATA")
public class ApprovalTemplateData {
	@Id
	@Column(name = "APPROVAL_TEMPLATE_DATA_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long approvalTemplateDataId;

	@Column(name = "EMPLOYEE_NAME")
	private String userName;

	@Column(name = "EMPLOYEE_ID")
	private String userid;
	
	@Column(name = "CREATED_TIME")
	private LocalDateTime createTime;

	@Column(name = "PROCESS_INSTANCE_ID")
	private String processInstanceId;
	
	@Column(name = "RESPONSE_DATA", length = 10000)
	private String responseData;

	@Column(name = "PROCESSCODE")
	private String processCode;
	
	@Column(name = "DEPARTMENT")
	private String departmentId; 

	public ApprovalTemplateData() {
		super();
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public long getApprovalTemplateDataId() {
		return approvalTemplateDataId;
	}

	public void setApprovalTemplateDataId(long approvalTemplateDataId) {
		this.approvalTemplateDataId = approvalTemplateDataId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getResponse() {
		return responseData;
	}

	public void setResponse(String responseData) {
		this.responseData = responseData;
	}
}

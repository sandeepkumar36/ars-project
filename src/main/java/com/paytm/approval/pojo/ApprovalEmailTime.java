package com.paytm.approval.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "EmailTime")
public class ApprovalEmailTime {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "APPROVAL_EMAILTIME_ID")
	private long approvalEmailTimeId;

	@Column(name = "EMAIL_TIME")
	private String timeForEmail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "APPROVAL_TEMPLATE_CONFIG_ID", nullable = false)
	private ApprovalTemplateConfig approvalTemplateConfigId;

	public long getApprovalEmailTimeId() {
		return approvalEmailTimeId;
	}

	public void setApprovalEmailTimeId(long approvalEmailTimeId) {
		this.approvalEmailTimeId = approvalEmailTimeId;
	}

	public ApprovalTemplateConfig getApprovalTemplateConfigId() {
		return approvalTemplateConfigId;
	}

	public void setApprovalTemplateConfigId(ApprovalTemplateConfig approvalTemplateConfigId) {
		this.approvalTemplateConfigId = approvalTemplateConfigId;
	}

	public String getTimeForEmail() {
		return timeForEmail;
	}

	public void setTimeForEmail(String timeForEmail) {
		this.timeForEmail = timeForEmail;
	}

}

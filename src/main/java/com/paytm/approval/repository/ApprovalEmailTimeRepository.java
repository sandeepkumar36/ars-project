package com.paytm.approval.repository;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.paytm.approval.pojo.ApprovalEmailTime;

@Repository
public interface ApprovalEmailTimeRepository extends JpaRepository<ApprovalEmailTime, Long> {

	@Query(value = "SELECT p.processCode as processCode ,p.duration as duration,p.fileName as filename, p.subject as subject,p.recievers as recievers FROM ApprovalTemplateConfig p JOIN ApprovalEmailTime aet on p.approvalTemplateConfigId = aet.approvalTemplateConfigId where aet.timeForEmail = :time")
	ArrayList< Map<String, Object> >  findBytimeForEmail(@Param("time") String time);
}

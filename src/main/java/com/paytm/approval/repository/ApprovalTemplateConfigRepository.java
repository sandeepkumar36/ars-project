package com.paytm.approval.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paytm.approval.pojo.ApprovalTemplateConfig;

@Repository
public interface ApprovalTemplateConfigRepository extends JpaRepository<ApprovalTemplateConfig, Long>{
	
	@SuppressWarnings("unchecked")
	ApprovalTemplateConfig save(ApprovalTemplateConfig config);
	
	List<ApprovalTemplateConfig> findAll();

}

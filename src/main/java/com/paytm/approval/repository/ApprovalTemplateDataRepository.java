package com.paytm.approval.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.paytm.approval.pojo.ApprovalTemplateData;

@Repository
public interface ApprovalTemplateDataRepository extends JpaRepository<ApprovalTemplateData, Long> {

	/**
	 * Query for fetching list of formData of processCode;
	 * 
	 */
	@Query(value = "SELECT userName as userName , userid as userId, createTime as createTime, responseData as responseData"
			+ " FROM ApprovalTemplateData "
			+ "atd WHERE createTime >= :startTime AND createTime <= :endTime AND processCode = :ProcessCode")
	List< Map<String,Object> >  findByProcessCodeAndCreateTime(String ProcessCode, LocalDateTime startTime,
			LocalDateTime endTime);

}

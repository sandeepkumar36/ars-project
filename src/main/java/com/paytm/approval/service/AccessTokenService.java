package com.paytm.approval.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.paytm.approval.utils.CommonMethod;

@Service
public class AccessTokenService {
	@Value("${ding.api.getAccessToken}")
	private String dingApiAccessToken;

	public Map<String, Object> getAccessToken() {
		return CommonMethod.hitThirdPartyUrl(dingApiAccessToken);
	}
}

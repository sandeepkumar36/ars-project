package com.paytm.approval.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.approval.pojo.ApprovalTemplateConfig;

public interface ApprovalService {
	/**
	 * This method is used to fetch department list in dingtalk organisation.
	 * 
	 * @author gouravsharma
	 * 
	 */
	Map<String, Object> getDepartmentList(String accessToken);

	/**
	 * This method is used to save Aprroval templateConfig
	 * 
	 * @param ApprovalTemplateConfig
	 */
	Map<String, Object> saveApprovalTemplateConfig(ApprovalTemplateConfig config);
	

	/***
	 * Funciton for creating excel sheet 
	 * @param query
	 * @param fileName
	 * @param subject
	 * @param recievers
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws MessagingException 
	 */
	void createCsvFileFuntion(List<Map<String, Object>> query, String fileName , String subject, String recievers) throws JsonParseException, JsonMappingException, IOException, MessagingException;

	void fetchDataFunction();

	
}

package com.paytm.approval.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.hibernate.exception.DataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytm.approval.constant.MessageConstant;
import com.paytm.approval.pojo.ApprovalTemplateConfig;
import com.paytm.approval.repository.ApprovalEmailTimeRepository;
import com.paytm.approval.repository.ApprovalTemplateConfigRepository;
import com.paytm.approval.repository.ApprovalTemplateDataRepository;
import com.paytm.approval.utils.CommonMethod;
import com.paytm.approval.utils.MiscUtils;

/**
 * 
 * Class for creating CSV file for each process code and sending to crossponding
 * emails
 * 
 */
@Service
public class ApprovalServiceImpl implements ApprovalService {

    @Value("${ding.api.getDepartmentList}")
    private String dingApiDepartmentList;

    @Value("${filepath}")
    private String filepath;

    @Autowired
    ApprovalTemplateConfigRepository approvalTemplateConfigRepository;

    @Autowired
    ApprovalEmailTimeRepository approvalEmailRepository;

    @Autowired
    ApprovalTemplateDataRepository approvalTemplateDataRepository;

    @Autowired
    private JavaMailSender javaEmailSender;

    Logger logger = LoggerFactory.getLogger(ApprovalServiceImpl.class);

    private static ObjectMapper mapper = null;
    private ArrayList<String> dataList = new ArrayList<>();
    private HSSFWorkbook workbook = null;
    private FileOutputStream fileOutStream = null;
    private Sheet sheet = null;
    private Font headerFont = null;
    private CellStyle cellStyle = null;
    private Cell cell = null;
    private Row row = null;

    @Override
    public Map<String, Object> getDepartmentList(String accessToken) {
	try {
	    String url = dingApiDepartmentList.replaceAll("\\$access_token\\$", "" + accessToken);
	    Map<String, Object> departmentList = CommonMethod.hitThirdPartyUrl(url);
	    Map<String, Object> map = MiscUtils.getSuccessResponse(MessageConstant.DEPARTMENT_LIST_FETCHED,
		    departmentList.get("department"));
	    return map;
	} catch (Exception ex) {
	    Map<String, Object> map = MiscUtils.getErrorResponse(ex.getLocalizedMessage());
	    logger.info("Error in fetching department list {}", ex.getMessage());
	    return map;
	}
    }

    private void sendDefaultMessage(String subject, String recievers) {
	MimeMessage message = javaEmailSender.createMimeMessage();
	try {

	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    String[] recieversList = recievers.split(",");
	    helper.setTo(recieversList);
	    helper.setSubject(subject);
	    helper.setText("No Record Found");
	    javaEmailSender.send(message);
	} catch (Exception e) {
	    logger.info("Error in Sending message {}", e.getMessage());
	}

    }

    private void sendEmailFunction(String fileName, String subject, String recievers) {

	MimeMessage message = javaEmailSender.createMimeMessage();
	try {

	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    String[] recieversList = recievers.split(",");
	    helper.setTo(recieversList);
	    helper.setSubject(subject);
	    helper.setText(subject);
	    fileName = filepath + fileName + ".xls";
	    FileSystemResource file = new FileSystemResource(new File(fileName));
	    File object = new File(fileName);
	    if (object.exists()) {
		logger.info("LOCATION OF CREATED FILE{}" + fileName);
		helper.addAttachment(fileName, file);
		javaEmailSender.send(message);
		object.delete();
	    }
	} catch (Exception e) {
	    logger.info("Error in Sending message {}", e.getMessage());
	}

    }

    @Override
    public Map<String, Object> saveApprovalTemplateConfig(ApprovalTemplateConfig config) {
	try {
	    return MiscUtils.getSuccessResponse(MessageConstant.SUCCESS_MESSAGE,
		    approvalTemplateConfigRepository.save(config));
	} catch (DataException ex) {
	    logger.info("Error in saving ApprovalTemplateConfig {}", ex.getMessage());
	    return MiscUtils.getErrorResponse(ex.getLocalizedMessage());
	}
    }

    /**
     * Function for creating csv file argument( processcode, startTime , endTime,
     * 
     * csv file format will be | EmployeeName | EmployeeId | FilledTime | formVal1 |
     * formVal2 ... |
     * 
     * @throws MessagingException
     * 
     */
    @Override
    public void createCsvFileFuntion(List<Map<String, Object>> list, String fileName, String subject, String recievers)
	    throws JsonParseException, JsonMappingException, IOException, MessagingException {

	/**
	 * for each index of arrayList we will have userName, userId, createdTime ,
	 * formData as JsonString first Make header then add row for each index of
	 * ArrayList
	 */
	if (mapper == null) {
	    mapper = new ObjectMapper(); // mapper.configure(Js)
	}

	if (!list.isEmpty()) {
	    workbook = new HSSFWorkbook();
	    fileOutStream = new FileOutputStream(filepath + fileName + ".xls");
	    sheet = workbook.createSheet("client");
	    dataList.add("EmployeeName");
	    dataList.add("EmployeeId");
	    dataList.add("FilledTime");
	    String res = (String) list.get(0).get("responseData");
	    List<FormDataInfo> formDataInfo = null;
	    try {
		formDataInfo = mapper.readValue(res, new TypeReference<List<FormDataInfo>>() {
		});
	    } catch (Exception e) {
		logger.info("Error in Json mapping {}", e.getMessage());
	    }
	    for (int i = 0; i < formDataInfo.size(); i++) {
		dataList.add(formDataInfo.get(i).getName());
	    }
	    makeHeaderForExcelSheet(dataList);
	    dataList.clear();

	    /***
	     * Now add row into created excel sheet
	     * 
	     */

	    for (int list_var = 0; list_var < list.size(); list_var++) {

		dataList.add((String) list.get(list_var).get("userName"));
		dataList.add((String) list.get(list_var).get("userId"));
		dataList.add(LocalDateTimeToStirng(list.get(list_var).get("createTime").toString()));
		res = (String) list.get(list_var).get("responseData");

		try {
		    formDataInfo = mapper.readValue(res, new TypeReference<List<FormDataInfo>>() {
		    });
		} catch (Exception e) {
		    logger.info("Error in Json mapping {}", e.getMessage());
		}

		for (int form_data_var = 0; form_data_var < formDataInfo.size(); form_data_var++) {

		    dataList.add(formDataInfo.get(form_data_var).getValue());
		}
		/*
		 * add data list at list_var +1 index
		 */
		addRowIntoExcelSheet(dataList, list_var + 1);
		dataList.clear();

	    }

	    workbook.write(fileOutStream);
	    fileOutStream.close();

	    /**
	     * send above excel sheet
	     */
	    try {
		sendEmailFunction(fileName, subject, recievers);
	    } catch (Exception e) {
		logger.info("Error in Sending Email {}", e.getMessage());
	    }
	} else {

	    /**
	     * their is no data so send default text;
	     */
	    sendDefaultMessage(subject, recievers);
	}
    }

    /**
     * Function for adding row in created excel sheet
     * 
     * @param fileName
     * @param subject
     * @param recievers
     */
    private void addRowIntoExcelSheet(ArrayList<String> dataList, int rowIndex) {
	row = sheet.createRow(rowIndex);
	headerFont = workbook.createFont();
	headerFont.setFontName("Arial");
	headerFont.setFontHeightInPoints((short) 12);
	cellStyle = workbook.createCellStyle();
	cellStyle.setFont(headerFont);
	logger.info("Row Data {},", dataList);
	for (int i = 0; i < dataList.size(); i++) {
	    cell = row.createCell(i);
	    cell.setCellValue(dataList.get(i));
	    sheet.autoSizeColumn(i);
	    cell.setCellStyle(cellStyle);
	}

    }

    /**
     * Function for creating header of excel sheet
     * 
     * @param headerList
     */
    private void makeHeaderForExcelSheet(ArrayList<String> headerList) {

	headerFont = workbook.createFont();
	headerFont.setBold(true);
	headerFont.setFontHeightInPoints((short) 14);
	headerFont.setColor(IndexedColors.BLACK.getIndex());
	logger.info("Header List {},", headerList);
	cellStyle = workbook.createCellStyle();
	cellStyle.setFont(headerFont);
	row = sheet.createRow(0);
	for (int i = 0; i < headerList.size(); i++) {
	    cell = row.createCell(i);
	    cell.setCellValue(headerList.get(i));
	    cell.setCellStyle(cellStyle);
	    sheet.autoSizeColumn(i);

	}

    }

    /**
     * funtion will convert LocalDataTime to String
     * 
     * @param localDataTime
     * @return
     */
    String LocalDateTimeToStirng(String createTime) {
	LocalDateTime now = LocalDateTime.parse(createTime);
	DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-dd-MM HH:mm:ss");
	return now.format(format);

    }

    /**
     * function task: will check time of each id: if time match with current time
     * the make csv and send it.
     */
    @Override
    public void fetchDataFunction() {

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

	String currentTime = simpleDateFormat.format(new Date());

	/**
	 * Fetch Approval template config data table on the basis of currenttime.
	 */
	ArrayList<Map<String, Object>> record = approvalEmailRepository.findBytimeForEmail(currentTime);
	logger.info("CurrentTime : {}", currentTime);
	logger.info("RecordList for currentTime {}", record);
	for (int record_var = 0; record_var < record.size(); record_var++) {
	    String processCode = (String) record.get(record_var).get("processCode");
	    String fileName = (String) record.get(record_var).get("filename");
	    String subject = (String) record.get(record_var).get("subject");
	    String recievers = (String) record.get(record_var).get("recievers");
	    int temp_var = (int) record.get(record_var).get("duration");
	    Long duration = (long) (temp_var);
	    LocalDateTime endTime = LocalDateTime.now();
	    LocalDateTime startTime = LocalDateTime.now().minusDays(duration);
	    List<Map<String, Object>> list = approvalTemplateDataRepository.findByProcessCodeAndCreateTime(processCode,
		    startTime, endTime);
	    logger.info("For " + record_var + " List " + list);
	    try {
		createCsvFileFuntion(list, fileName, subject, recievers);
	    } catch (Exception e) {
		logger.info("Error in Creating CSV file {}", e.getMessage());
	    }
	}
    }

}

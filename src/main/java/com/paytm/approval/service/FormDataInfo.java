package com.paytm.approval.service;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class for mapping formData 
 */

public class FormDataInfo {

	private String name;
	
	@JsonProperty(defaultValue = "")
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "[name=" + name + ", value=" + value + "]";
	}

}

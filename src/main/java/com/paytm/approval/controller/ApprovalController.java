package com.paytm.approval.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.paytm.approval.pojo.ApprovalTemplateConfig;
import com.paytm.approval.service.ApprovalService;
import com.paytm.approval.utils.AccessToken;

@RestController
public class ApprovalController {

	@Autowired
	ApprovalService approvalService;
	
	Logger logger = LoggerFactory.getLogger(ApprovalController.class);

	@GetMapping("/getDepartmentList")
	public Map<String, Object> getDepartmentList() {
		String accessToken = AccessToken.getInstance().getToken();
		return approvalService.getDepartmentList(accessToken);
	}

	@PostMapping("/appConfig")
	public Map<String, Object> saveTemplateConfig(@RequestBody ApprovalTemplateConfig config) {
	        logger.info("Request for Save Template Data Config!!");
		return approvalService.saveApprovalTemplateConfig(config);
	}

	@Scheduled(fixedDelay = 60000) 
	public void  sendMessage() {
		logger.info("Request to send csv to receiver!!");
		approvalService.fetchDataFunction();
	}
}
